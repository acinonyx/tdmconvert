"""
This module provides methods to convert positional observations from
the IOD format into the CCSDS TDM format.

The IOD Observation Format Description is provided at
http://www.satobs.org/position/IODformat.html

CCSDS Tracking Data Messages (CCSDS 503.0-B-2, published June 2020):
https://public.ccsds.org/Pubs/503x0b2c1.pdf
"""
import logging
from collections import defaultdict
from pathlib import Path

from beyond.dates import Date
from beyond.io.ccsds import dumps
from beyond.utils.measures import Declination, MeasureSet, RightAscension

from .iod_parser import IODParseError, parse_iod


def read_iod_lines(iod_str):
    '''
    Read passive optical tracking data from IOD line data and output a dict of
    beyond.utils.measures.MeasureSets, grouped by path (defined by NORAD ID and Site ID).

    To dump those measurements to TDM files the beyond.io.ccsds.dumps method can be used.

    :param iod_str: IOD input data
    :type iod_str: str

    :return: List of beyond.utils.measures.MeasureSets
    :rtype: list[str]
    '''
    measures_collection = defaultdict(MeasureSet)

    for line in iod_str.strip().split('\n'):
        try:
            record = parse_iod(line)
        except IODParseError:
            if not line[0].isdigit() or len(line) <= 2:
                # Certainly not an IOD line, skip.
                pass

            # This line Might be a false negative
            logging.debug("Skipped: %s", line)
            continue

        path = [str(record['norad_id']), str(record['site_id'])]
        path_str = f'{record["norad_id"]}-{record["site_id"]}'

        date = Date(record['datetime'])
        measures_collection[path_str].append(RightAscension(path, date, record['ra']))
        measures_collection[path_str].append(Declination(path, date, record['dec']))
    return measures_collection


def iod2tdm(iod_str, tdm_format, originator='N/A'):
    '''
    Wrapper around `read_iod_lines`. Returns the measurements as CCSDS TDM files.

    :param iod_str: IOD input data
    :param output_filename: filename for the output CCSDS TDM datafile
    :param tdm_format: Output format of the file, can be 'xml' or 'kvn'.
    :param originator: Creating agency. Value should be an entry from the ‘Abbreviation’
                       column in the SANA Organizations Registry,
                       https://sanaregistry.org/r/organizations/organizations.html
    :type iod_str: str
    :type output_filename: str
    :type tdm_format: str
    :type originator: str

    :return: List of TDM messages
    :rtype: list[str]
    '''
    measures_collection = read_iod_lines(iod_str)
    tdms = []

    for _, measures in measures_collection.items():
        tdms.append(
            dumps(measures, fmt=tdm_format, creation_date=Date.now(), originator=originator))

    return tdms


def iod2tdm_file(iod_str, output_filename, tdm_format, originator='N/A'):
    """
    Wrapper around `iod2tdm`.

    Assumes that all measurements are for the same path (identical NORAD ID / Site ID pair).
    Writes them to a single CCSDS TDM file.

    .. deprecated:
       This method fails when the assertion is not true. The method iod2tdm does not exhibit
       this problem and thus should be used instead.
    """
    tdms = iod2tdm(iod_str, tdm_format, originator)

    Path(output_filename).write_text(tdms[0])
