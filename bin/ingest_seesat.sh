#!/bin/bash

TASK_ID=$(docker-compose exec ingestion_service celery call app.ingest_seesat)

docker-compose exec ingestion_service celery result "$TASK_ID"
TASK_EXIT_CODE=$?

docker-compose logs ingestion_service \
    | grep "$TASK_ID" \
    | sed 's/.*\] //g' \
    | sed 's/.*\]: //g' \
    | grep -v "^$"

exit $TASK_EXIT_CODE
