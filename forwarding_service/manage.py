"""
This module provides the commands to control the forwarding service.
"""
# Pylint:
# - Disable "no name app in module app" false positive
# pylint: disable=no-name-in-module

from flask.cli import FlaskGroup

from app import app

CLI = FlaskGroup(app)

if __name__ == "__main__":
    CLI()
