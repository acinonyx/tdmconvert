"""Basic tests for the conversion from STRF datafiles to CCSDS TDM files.
"""

from pathlib import Path

from tdmconvert import strf2tdm
from utils import assert_string

folder = Path(__file__).parent / "data"

IGNORE_STR = ['CREATION_DATE']


def test_strf2tdm(tmp_path):
    tmp_filename = tmp_path.joinpath('ccsds_tdm_5304653.xml')
    output_filename = folder.joinpath('ccsds_tdm_5304653.xml')

    strf2tdm(doppler_filename=folder.joinpath('strf_5304653.dat'),
             output_filename=tmp_filename,
             tdm_format='xml',
             obs_metadata={
                 'satellite_id': 'EQAP-0335-6677-5204-0983',
                 'frequency_tx': 435502923.076,
                 'ground_station_id': 2012
             },
             originator='LSF')

    assert_string(tmp_filename.read_text(), output_filename.read_text(), ignore=IGNORE_STR)
