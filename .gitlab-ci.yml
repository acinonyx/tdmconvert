variables:
  GITLAB_CI_IMAGE_ALPINE: 'alpine:3.17.3'
  GITLAB_CI_IMAGE_PYTHON: 'python:3.10'
  GITLAB_CI_IMAGE_DOCKER: 'docker:23.0.4'
  GITLAB_CI_PYPI_TOX: 'tox~=3.24.5'
  GITLAB_CI_SIGN_OFF_EXCLUDE: '880d698f23a7279d53d3dc7482c4ba328337f430'
  GITLAB_CI_DOCKER_BUILDX_PLATFORMS: 'linux/amd64'
stages:
  - static
  - test
  - deploy

# 'static' stage
sign_off:
  stage: static
  needs: []
  image: ${GITLAB_CI_IMAGE_ALPINE}
  before_script:
    - apk add --no-cache git
  script: >-
    git log
    --grep "^Signed-off-by: .\+<.\+\(@\| at \).\+\(\.\| dot \).\+>$"
    --invert-grep
    --format="Detected commit '%h' with missing or bad sign-off! Please read 'CONTRIBUTING.md'."
    --exit-code
    $(rev=$(git rev-parse -q --verify "$GITLAB_CI_SIGN_OFF_EXCLUDE^{commit}") && echo "$rev..")

static:
  stage: static
  needs: []
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - pip install "$GITLAB_CI_PYPI_TOX"
  script:
    - tox -e "flake8,isort,yapf,pylint"

# 'test' stage
test:
  stage: test
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - pip install "$GITLAB_CI_PYPI_TOX"
  script:
    - tox -e "pytest"

# 'deploy' stage
docker:
  stage: deploy
  image: ${GITLAB_CI_IMAGE_DOCKER}-cli
  variables:
    GNURADIO_IMAGE_TAG: 'satnogs'
  services:
    - ${GITLAB_CI_IMAGE_DOCKER}-dind
  script:
    - cp env-dist .env
    - |
      cat << EOF > /etc/buildkitd.toml
      [worker.oci]
      max-parallelism = 1
      EOF
    - >-
      docker buildx
      create
      --config /etc/buildkitd.toml
      --use
      --name container
      --driver docker-container
      --bootstrap
    - '[ -z "$CI_REGISTRY_IMAGE" ] || docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY'
    - 'CACHE_IMAGE="${CI_REGISTRY_IMAGE:+$CI_REGISTRY_IMAGE/ingestion_service:cache}"; export CACHE_IMAGE'
    - >-
      docker buildx
      bake
      --progress plain
      -f docker-compose.yml
      ${CI_REGISTRY_IMAGE:+ -f docker-compose.cache.yml}
      --pull
      --set "*.platform=${GITLAB_CI_DOCKER_BUILDX_PLATFORMS}"
      ${CACHE_IMAGE:+ --set "*.cache-to=type=registry,ref=$CACHE_IMAGE"}
      ${CI_REGISTRY_IMAGE:+ --set "*.tags=$CI_REGISTRY_IMAGE/ingestion_service:${CI_COMMIT_REF_NAME}"}
      ${CI_COMMIT_TAG:+${CI_REGISTRY_IMAGE:+ --set "*.tags=$CI_REGISTRY_IMAGE/ingestion_service:latest"}}
      ${CI_REGISTRY_IMAGE:+ --push}
      ingestion_service
    - 'CACHE_IMAGE="${CI_REGISTRY_IMAGE:+$CI_REGISTRY_IMAGE/forwarding_service:cache}"; export CACHE_IMAGE'
    - >-
      docker buildx
      bake
      --progress plain
      -f docker-compose.yml
      ${CI_REGISTRY_IMAGE:+ -f docker-compose.cache.yml}
      --pull
      --set "*.platform=${GITLAB_CI_DOCKER_BUILDX_PLATFORMS}"
      ${CACHE_IMAGE:+ --set "*.cache-to=type=registry,ref=$CACHE_IMAGE"}
      ${CI_REGISTRY_IMAGE:+ --set "*.tags=$CI_REGISTRY_IMAGE/forwarding_service:${CI_COMMIT_REF_NAME}"}
      ${CI_COMMIT_TAG:+${CI_REGISTRY_IMAGE:+ --set "*.tags=$CI_REGISTRY_IMAGE/forwarding_service:latest"}}
      ${CI_REGISTRY_IMAGE:+ --push}
      forwarding_service

  rules:
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
