# TDM Converter

tdmconvert is an open source (GPLv3) Python library to convert optical (IOD) and RF (STRF) satellite tracking measurements to the common CCSDS TDM (Tracking Data Message) Format.
Additionally a set of services is provided to fetch IOD measurements by a selected set of observers from the Seesat-L mailing list, convert them to TDM messages and forward them to 3rd party services.

## Library `tdmconvert`
### Usage

- Parsing a single IOD measurement into a dictionary object:
  ```python
  from tdmconvert.iod_parser import IODParseError, parse_iod
  try:
    message = parse_iod("40978 15 058Q   4171 E 20220508204215567 17 25 0414004+855477 37 R")
  except IODparseError as e:
    print(e)
  ```

- Parse multiple IOD measurements (e.g. from an email) into a list of MeasureSets group by path (NORAD ID / site ID pairs):
  ```python
  from tdmconvert import read_iod_lines

  iod_str = """These are my measurements:
  14240 83 079A   0797 G 20200915193902868 17 25 1210942+754702 37 S
  14240 83 079A   0797 G 20200915193931933 17 25 1037332+745668 37 S
  11693 80 012C   0797 G 20200915194402636 17 25 1240541+722918 37 S
  11693 80 012C   0797 G 20200915194413440 17 25 1252347+731870 37 S
  """
  measures_collection = read_iod_lines(iod_str)
  print("Paths:")
  for path, measures in measures_collection.items():
      print(path)
  ```

- Convert an IOD message to a CCSDS TDM message string:
  ```python
  from tdmconvert import iod2tdm

  iod_str = "40978 15 058Q   4171 E 20220508204215567 17 25 0414004+855477 37 R"
  tdm_str = iod2tdm(iod_str, tdm_format='kvn', originator='LSF')

  print(tdm_str)
  ```

## Services
### Overview

| Service             | Port | Exposed  | Description                            |
|-|-|-|-|
| `ingestion_service` | - | - | Fetched mails from seesat-l and uploads them to the `forwarding_service` |
| `forwarding_service`  | 5000 | optional | Forwarding Service REST API            |
| `redis`               | 6379 | No       | Celery Backend for the `ingestion_service`  |
| `prometheus`        | 9090 | optional | Prometheus Web GUI                     |
| `pushgateway`       | 9091 | No       | Pushgateway collection endpoint        |
| `grafana`           | 3000 | optional | Grafana Web GUI                        |


#### Usage

- Build the service containers
  ```
  docker-compose build
  ```

- Start the service containers
  ```
  docker-compose up -d
  ```

### TDM Ingestion Service (`ingestion_service`)

Fetch IOD measurements from SeeSat-L mailing list, convertm them to TDM messages and forward them to the TDM Forwading Service.

#### Usage

- Manually run the seesat-l ingestion task
  ```
  ./bin/ingest_seesat.sh
  ```


### TDM Forwarding Service (`forwarding_service`)

Receive Tracking Data Messages and forward them to registered third party services.

#### Endpoints

This service provides only one endpoint currently.

- PUT `/forward_tdms/`
  Forward all submitted CCSDS TDMs to all configured third-party receivers.

  Parameters:
  - `tdms`: list of strings, each entry representing one CCSDS TDM file, UTF-8 encoded
  
  Example usage (via Python):
  ```python3
  import requests
  
  from pathlib import Path
  
  
  tdm_file = Path("./tests/data/ccsds_tdm_5304653.xml").read_text(encoding='utf-8')
  url = "http://0.0.0.0:5000/forward_tdms"
  
  json_body = {'tdms': [tdm_file]}
  response = requests.put(url, json=json_body)
  ```

#### OpenAPI Specification

This internal API is documented in the openAPI specs in the `./docs` directory.

#### Monitoring

The following metrics get exported by a Prometheus Pushgateway:
- `seesat_ingest_job_duration_seconds_count`: Count of observed seesat ingest jobs.
- `seesat_ingest_job_duration_seconds_sum`: Total sum of seconds observed to have been spent on seesat ingest jobs.
- `seesat_ingest_job_duration_seconds_created`: Value of the unix timestamp for when the metric was created.
- `seesat_ingest_messages_total`: Count of email messages from allowlisted submitters seen during seesat ingest.
- `seesat_ingest_datasets_total`: Count of datasets (message, norad id, site id pairs) seen during seesat ingest.
- `seesat_ingest_measurements_total`: Count of (IOD) measurements seen during seesat ingest.
- `seesat_ingest_failed_tasks_total`: Count of failed ingest tasks.

# License

AGPL-3.0
